<?php

/*
Plugin Name: M23 Filter
Version: 0.1
Description: Foo
Plugin URI:
Author: Martin Wecke
Author URI: http://martinwecke.de/
Bitbucket Plugin URI:
Bitbucket Branch: master
*/


class M23Filter {
    public function __construct() {
        $this->init();
    }


    /**
     * Init
     */
    public function init() {
        // Save post hook
        add_action( 'save_post', array( $this, 'savePost' ), 100, 1 ); 

        // Register custom query vars 
        add_filter( 'query_vars', array( $this, 'registerQueryVars' ) );

        // Register custom rewrite rules
        add_action( 'init', array( $this, 'registerRewriteRules' ) );

        // Modify query
        add_filter( 'pre_get_posts', array( $this, 'modifyQuery' ) );
    }


    /**
     * Save filter token in meta field
     * @param  integer $post_id post ID
     */
    public function savePost( $post_id ) {
        $post = get_post( $post_id );

        // quit if post is post revisions
        if( wp_is_post_revision( $post_id ) ) {
            return;
        }

        // quit if post type is not 'works'
        if( $post->post_type !== 'works' ) {
            return;
        }

        // quit if post is trash / draft / auto draft
        if( $post->post_status === 'trash' || $post->post_status === 'draft' || $post->post_status === 'auto-draft' ) {
            return;
        }

        $token = '';

        // title
        $token .= get_the_title( $post_id ) . ' ';

        // content
        $token .= $post->post_content . ' ';

        // tags 
        $tags = get_the_terms( $post, 'work_tags' );
        foreach( $tags as $tag ) {
            $token .= $tag->name. ' ';            
        }

        // save token as meta field
        update_post_meta( $post_id, 'filter--token', $token );
    }


    /**
     * Register custom query vars
     * @param  array $vars current query vars
     * @return array       modified query vars
     */
    function registerQueryVars( $vars ) {
        // templates
        $vars[] = 'filter';  

        return $vars;
    }    


    /**
     * Add custom rewrite rules to translate permalinks
     * to custom query vars
     */
    function registerRewriteRules() {
        global $wp_rewrite;

        // filter 
        add_rewrite_rule( 
            'works/filter/([^/]*)/?$', 
            'index.php?post_type=works&filter=$matches[1]', 
            'top' 
        );       
    }


    function modifyQuery( $query ) {
        if( is_admin() ) {
            return $query;
        }

        if( !is_archive || get_post_type() === 'works' ) {
            return $query;            
        }

        if( $filter = get_query_var( 'filter' ) ) {
            $filter = explode( ',', $filter );

            $meta = $query->query_vars['meta_query'];
            if( !$meta ) {
                $meta = array();
            }

            $meta['relation'] = 'AND';

            foreach( $filter as $word ) {
                $word = str_replace( '+', ' ', $word );
                $meta[] = array(
                    'key' => 'filter--token',
                    'value' => $word,
                    'compare' => 'LIKE'
                );
            }

            $query->query_vars[ 'meta_query' ] = $meta;
        }

        return $query;
    }

    public static function getQueryString() {
        $string = '';

        $query_var = get_query_var( 'filter' );
        
        if( !$query_var ) {
            return $string;
        }

        $string = str_replace( ',', ' ', $query_var );
        $string = str_replace( '+', ' ', $string );

        return $string;
    }
}
 

new M23Filter();